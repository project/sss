-- INTRODUCTION --

The Smart Service System (sss) module for Drupal is a 'step towards a science of service systems' for buildings.

"The service sector accounts for most of the world’s economic activity, but it’s the least-studied part of the economy. A service system comprises people and technologies that adaptively compute and adjust to a system’s changing value of knowledge. A science of service systems could provide theory and practice around service innovation."

(ref: Spohrer, J., Maglio, P., Bailey, J., Gruhl, D. (2007) Steps towards a science of service systems. Computer 40(1):71-77. doi:10.1109/MC.2007.33)

-- PURPOSE --

The purpose of the SSS module is to provide students a quick and easy way to setup a local install of Drupal with a few functional components important to the application of service system science to BUILDINGS.

Service integration projects largely require interdisciplinary interactions between groups that value secrecy or at least restrict access to enterprise systems on a 'need to know' basis.  Visualizing integration projects to building automation systems is a formidable challenge even for experts on the inside.  Such boundaries make it impossible to mock solutions when you do not know whats there.

The SSS module bridges this gap by providing a simple database cache to pre-populate real-time data elements.  These real-time elements (ie. numerical values, blocks, views, charts and graphs) are the building blocks for smart sites.  By simulating such service systems for buildings, web designers and developers can focus on the work of integrating 'smart building' content into the site's front page, landing pages, and as many building detail pages as may be warranted.


The objective is to practice presenting real-time building data of worth, aka 'cool points', to the anonymous user. Success can be measured when it enhances the user experience of site visitors as well as the business objectives of the site owner.

Anonymous users to web sites greatly outnumber authenticated users.  Improved content will increase the number of users to the site.  Mixing 'smart building' content into the content mix on high traffic pages will make users smarter about the facilities they occupy and the building systems they use.  The 'smart site' will make users smarter just as the 'smart phone' does.  It gives them quick access to accurate information to better guide their actions.

-- GENERAL --

SSS provides external library handling for Drupal modules.

*  For a full description visit the project page @ http://drupal.org/project/sss

*  For bug reports, feature suggestions and latest developments visit the issues queue @ http://drupal.org/project/issues/sss


-- REQUIREMENTS --

WARNING:  The 7.x-1.x branch of this module is designed specifically for LOCALLY hosted environments ONLY.  It is not intended for installation in public hosting environments such as Acquia, Pantheon or others.  It has been tested there and DOES NOT WORK.  A secure 7.x-2.x branch is planned specifically for such shared hosting environments.

* Drupal 7.x
* Chaos tool suite (ctools)
* Views
* Libraries 2.0
* Flots 0.7 library  (see https://code.google.com/p/flot/downloads/detail?name=flot-0.7.zip&can=2&q=)


-- INSTALLATION --

Install as usual, see https://www.drupal.org/documentation/install/modules-themes/modules-7 for further information.
  
*  Note that installing external libraries is separate from installing this module and should happen in the sites/all/libraries directory. See http://drupal.org/node/1440066 for more information.


-- Libraries Installation --

Copy each of the following libraries into their respective folder (i.e. flot and transform) in the libraries folder for the module to detect the libraries correctly

* Flots 0.7 library  (see https://code.google.com/p/flot/downloads/detail?name=flot-0.7.zip&can=2&q=)

* Jquery transform 0.9.3 library ( see http://wiki.github.com/heygrady/transform/ )


-- CONTACT --

Current maintainers:

* David Thompson (dbt102) - https://www.drupal.org/user/338300