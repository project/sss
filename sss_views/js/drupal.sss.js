(function($) {

  $.fn.sssTableData = function() {
    return $(this).find('tbody tr').map(function() {
      var row = $(this)
      var time = new Date(row.find('th').text());
      time = time - (time.getTimezoneOffset() * 60000);
      return [[
        time,
        row.find('td').text()
      ]];
    });
  };

  $.fn.sssPlotGraph = function(data) {
    var graph = $('<div class="sss-trend-graph"/>').css({
      height: 400,
      width: "100%"
    });

    var view = $(this);

    var isBarGraph = view.find('.sss-table-graph').hasClass('bar-graph');

    view
      .after(graph)
      .hide();

    $.plot(graph, data, {
      lines: { show: !isBarGraph, lineWidth: 3 },
      bars: { show: isBarGraph, lineWidth: 3 },
      xaxis: { mode: "time", format: "%H:%M", twelveHourClock: true }
    });
  }

  $.fn.sssLineLabel = function(unit) {
    return $(this).find('caption').text() + " in " + unit;
  }

  Drupal.behaviors.sss = {
    attach: function(context) {
      $(context).find('.sss-trend').each(function() {
        var viewWrapper = $(this);
        var data = [];
        var unit = viewWrapper.data('unit');

        if (viewWrapper.hasClass('consolidated-rows')) {
          data = $(this).find('.sss-table-graph').map(function() {
            var table = $(this);
            return {
              data: table.sssTableData(),
              label: table.sssLineLabel(unit)
            };
          });

          viewWrapper.sssPlotGraph(data);
        }

        else {
          viewWrapper.find('.sss-table-graph').each(function() {
            var table = $(this);
            data = [{
              data: table.sssTableData(),
              label: table.sssLineLabel(unit)
            }];

            table.sssPlotGraph(data);
          });
        }
      }).end()
      .find('.sss-value').each(function() {
        var viewWrapper = $(this);
        var rangeMin = viewWrapper.data('range-min');
        var rangeMax = viewWrapper.data('range-max');
        var gaugeCircumference = (rangeMax - rangeMin) * 2;
        var radius = gaugeCircumference / (2 * Math.PI);
        var unit = viewWrapper.data('unit');

        viewWrapper.find('.sss-dial').each(function() {
          var dial = $(this);
          var val = dial.text();
          var dialComponents;

          dial.addClass('sss-dial-processed');

          dialComponents = "<div class='arrow'></div>\
            <div class='value'>" + val + " " + unit + "</div>\
            <div class='min'>" + rangeMin + "</div>\
            <div class='max'>" + rangeMax + "</div>";
			//alert(dialComponents)

          dial.html(dialComponents).find('.arrow').transform({
            origin: ['177px', '221px'],
            rotate: (((val / radius) * (180 / Math.PI)) - 90) + 'deg'
          });
        });
      });
    }
  };
})(jQuery);
