<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $options - values selected in the views style Settings
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */

  // for each data array
foreach ( $sss_views_data as $data ) {
?>
          <table class="sss-table-graph">
            <caption><?php print $row->title;?></caption>
            <thead>
              <tr>
                <th scope="col">Date</th>
                <th scope="col">Energy usage</th>
              </tr>
            </thead>
            <tbody>
<?php
    // loop through data array
  for ($i = 0; $i < count($data); $i = $i + 2 ) {
?>
              <tr>
                <th scope="row"><?php print $data[$i]; ?></th>
                <td><?php print $data[$i + 1]; ?></td>
              </tr>
<?php
  } // end of for
?>
            </tbody>
          </table>
<?php
} // end of foreach
