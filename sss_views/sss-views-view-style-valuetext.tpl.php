<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $options : user defined style options 
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $options - values selected in the views style Settings
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
  <div <?php print ($options['css_classes'] != '') ? 'class="'.$options['css_classes'].'"' : '' ?>>
  <?php foreach ($rows AS $row_key => $row_value): ?>
    <?php print $row_value; ?>
  <?php endforeach; ?>
  <?php print $options['units']; ?>
  </div>
