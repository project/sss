<?php

/**
 * Implementation of hook_views_plugins().
 */
function sss_views_views_plugins() {
  return array(
    'module' => 'sss_views',
    'style' => array(
      'sss_views_valuetext' => array(
        'title' => t('sss_views Value as Text'),
        'help' => t('Displays a single sss_views value.'),
        'handler' => 'sss_views_plugin_style_valuetext',
        'theme' => 'sss_views_view_style_valuetext',
        'uses row plugin' => TRUE ,
//        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),

      'sss_views_valuegraphic' => array(
        'title' => t('sss_views Value as a Graphic'),
        'help' => t('Displays a single sss_views value as a graphic.'),
        'handler' => 'sss_views_plugin_style_valuegraphic',
        'theme' => 'sss_views_view_style_valuegraphic',
        'uses row plugin' => TRUE ,
//        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),

      'sss_views_trend' => array(
        'title' => t('sss_views Trend Data'),
        'help' => t('Displays sss_views Trend data.'),
        'handler' => 'sss_views_plugin_style_trend',
        'theme' => 'sss_views_view_style_trend',
        'uses row plugin' => TRUE ,
//        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    ),
    'row' => array(
      'sss_views_bargraph' => array(
        'title' => t('Bar Graph'),
        'help' => t('sss_views trend data displayed a bar graph.'),
        'handler' => 'sss_views_plugin_row_bargraph',
        'theme' => 'sss_views_view_row_bargraph',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'help topic' => 'style-node',
      ),
      'sss_views_linegraph' => array(
        'title' => t('Line Graph'),
        'help' => t('sss_views trend data displayed a line graph.'),
        'handler' => 'sss_views_plugin_row_linegraph',
        'theme' => 'sss_views_view_row_linegraph',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'help topic' => 'style-node',
      ),

      'sss_views_dial' => array(
        'title' => t('Dial'),
        'help' => t('sss_views single value displayed as a dial.'),
        'handler' => 'sss_views_plugin_row_dial',
        'theme' => 'sss_views_view_row_dial',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'help topic' => 'style-node',
      ),
      'sss_views_text' => array(
        'title' => t('Text/String'),
        'help' => t('sss_views single value displayed as text.'),
        'handler' => 'sss_views_plugin_row_text',
        'theme' => 'sss_views_view_row_text',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'help topic' => 'style-node',
      ),


    )
  );
} // end of function sss_views_views_plugins