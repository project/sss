<?php

/**
 * @file
 * sss_views row plugin.
 * Kudos to the GMap module for being the basis for this implementation
 */

/**
 * Row plugin to render a trend graph.
 *
 * @ingroup views_row_plugins
 */
class sss_views_plugin_row_text extends views_plugin_row {

  /**
   * Initialize the row plugin.
   */
  function init(&$view, &$display, $options = NULL) {
    $this->view = &$view;
    $this->display = &$display;

    // Overlay incoming options on top of defaults
    $this->unpack_options($this->options, isset($options) ? $options : $display->handler->get_option('row_options'));
  }

  function uses_fields() {
    return !empty($this->definition['uses fields']);
  }


  /**
   * Render a row object. This usually passes through to a theme template
   * of some form, but not always.
   */
  function render($render) {
	$row = $render['row'];
	$fields = (isset($render['fields']))? $render['fields'] : "";
	$sss_views_data = $render['sss_views_data'];
	
    $row_output = theme( $this->theme_functions(),
      array(
        'view' => $this->view,
        'options' => $this->options,
        'row' => $row,
        'fields' => $fields,
        'sss_views_data' => $sss_views_data,
        'field_alias' => isset($this->field_alias) ? $this->field_alias : '',
      ));

    return $row_output;
  } // end of function, render

} // end of class, sss_views_plugin_row_sss_viewstrendgraph


/**
 * Template preprocessing for the row theme
 */
function sss_views_preprocess_sss_views_view_row_text(&$vars) {
  
} // end of function
