<?php

/**
 * @file
 * sss_views style plugin.
 * Kudos to the GMap module for being the basis for this implementation
 */

/**
 * Style plugin to render a trend graph.
 *
 * @ingroup views_style_plugins
 */
class sss_views_plugin_style_trend extends views_plugin_style {

  /**
   * Store all availible tokens row rows.
   */
  var $row_tokens = array();
  /**
   * Initialize a style plugin.
   *
   * @param $view
   * @param $display
   * @param $options
   *   The style options might come externally as the style can be sourced
   *   from at least two locations. If it's not included, look on the display.
   */
  function init(&$view, &$display, $options = NULL) {
    $this->view = &$view;
    $this->display = &$display;

    // Overlay incoming options on top of defaults
    $this->unpack_options($this->options, isset($options) ? $options : $display->handler->get_option('style_options'));

    if ($this->uses_row_plugin() && $display->handler->get_option('row_plugin')) {
      $this->row_plugin = $display->handler->get_plugin('row');
    }

    $this->options += array(
      'grouping' => '',
    );

    $this->definition += array(
      'uses grouping' => TRUE,
    );
  }

  function destroy() {
    parent::destroy();

    if (isset($this->row_plugin)) {
      $this->row_plugin->destroy();
    }
  }

  /**
   * Return TRUE if this style also uses a row plugin.
   */
  function uses_row_plugin() {
    return !empty($this->definition['uses row plugin']);
  }

  /**
   * Return TRUE if this style also uses a row plugin.
   */
  function uses_row_class() {
    return !empty($this->definition['uses row class']);
  }

  /**
   * Return TRUE if this style also uses fields.
   */
  function uses_fields() {
    // If we use a row plugin, ask the row plugin. Chances are, we don't
    // care, it does.
    if ($this->uses_row_plugin() && !empty($this->row_plugin)) {
      return $this->row_plugin->uses_fields();
    }
    // Otherwise, maybe we do.
    return !empty($this->definition['uses fields']);
  }

  /**
   * Return TRUE if this style uses tokens.
   *
   * Used to ensure we don't fetch tokens when not needed for performance.
   */
  function uses_tokens() {
    if ($this->uses_row_class()) {
      $class = $this->options['row_class'];
      if (strpos($class, '[') !== FALSE || strpos($class, '!') !== FALSE || strpos($class, '%') !== FALSE) {
        return TRUE;
      }
    }
  }


  /**
   * Return the token replaced row class for the specified row.
   */
  function get_row_class($row_index) {
    if ($this->uses_row_class()) {
      $class = $this->options['row_class'];
      if ($this->uses_fields() && $this->view->field) {
        $class = strip_tags($this->tokenize_value($class, $row_index));
      }

      $classes = explode(' ', $class);
      foreach ($classes as &$class) {
        $class = drupal_clean_css_identifier($class);
      }
      return implode(' ', $classes);
    }
  }


  /**
   * Take a value and apply token replacement logic to it.
   */
  function tokenize_value($value, $row_index) {
    if (strpos($value, '[') !== FALSE || strpos($value, '!') !== FALSE || strpos($value, '%') !== FALSE) {
      $fake_item = array(
        'alter_text' => TRUE,
        'text' => $value,
      );

      // Row tokens might be empty, for example for node row style.
      $tokens = isset($this->row_tokens[$row_index]) ? $this->row_tokens[$row_index] : array();
      if (!empty($this->view->build_info['substitutions'])) {
        $tokens += $this->view->build_info['substitutions'];
      }

      if ($tokens) {
        $value = strtr($value, $tokens);
      }
    }

    return $value;
  }

  /**
   * Should the output of the style plugin be rendered even if it's a empty view.
   */
  function even_empty() {
    return !empty($this->definition['even empty']);
  }

  /**
   * Set default options
   */
  function option_definition() {
//    $options = parent::option_definition();

    $options['rows_consolidation'] = array('default' => 'individual-rows');
    $options['units'] = array('default' => 'kwh');
    $options['timestamp_adjustment'] = array('default' => 0);
    $options['interval'] = array('default' => 'day');
    $options['max_records'] = array('default' => 100);
    $options['end_date_range'] = array('default' => 'now' );

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
//    parent::options_form($form, $form_state);

    $form['rows_consolidation'] = array(
      '#type' => 'select',
      '#title' => t('Consolidation Choose'),
      '#options' => array('consolidated-rows' => t('Render Rows as a Consolidated Row Object'), 'individual-rows' => t('Render Each Row Independently')),
      '#default_value' => $this->options['rows_consolidation'],
    );

    $form['units'] = array(
      '#type' => 'select',
      '#title' => t('Units'),
      '#options' => array('kwh' => t('KWh (Consumption)'), 'kw' => t('KW (Usage)')),
      '#default_value' => $this->options['units'],
    );
    $form['interval'] = array(
      '#type' => 'select',
      '#title' => t('Interval'),
      '#options' => array('hour' => t('Hours'), 'day' => t('Days'), 'week' => t('Week (not implemented)'), 'month' => t('Month'), 'year' => t('Year'), 'timestamp' => t('Actual Timestamps (not implemented)') ),
      '#description' => t('What values will be shown on the X axis?'),
      '#default_value' => $this->options['interval'],
    );
    $form['max_records'] = array(
      '#title' => t('Maximum Records'),
      '#description' => t('How many records should be requested from B-AWS.'),
      '#type' => 'textfield',
      '#size' => '4',
      '#default_value' => $this->options['max_records'],
    );
    $form['end_date_range'] = array(
      '#type' => 'select',
      '#title' => t('Date Range End Point'),
      '#description' => t('When should the trend data end?'),
      '#options' => array('now' => 'Now', '-1 hour' => t('1 Hour Ago'), '-1 day' => t('1 Day Ago'), '-1 week' => t('1 Week Ago'), '-1 month' => t('1 Month Ago'), '-1 year' => t('1 Year Year') ),
      '#default_value' => $this->options['end_date_range'],
    );
    $form['timestamp_adjustment'] = array(
      '#type' => 'select',
      '#title' => t('The timestamps of samples occur'),
      '#options' => array('0' => t('Immediate before the period ends - No Adjustment'), '1' => t('Immediate after the period ends - Adjust by 1 period')),
      '#description' => t('For example July Consumption data recorded at 08/01/2011 00:00:01 would need to be adjusted to avoid July (07) data showing as August (08)'),
      '#default_value' => $this->options['timestamp_adjustment'],
    );

  }


  /**
   * Render all of the fields for a given style and store them on the object.
   *
   * @param $result
   *   The result array from $view->result
   */
  function render_fields($result) {

    if (!$this->uses_fields()) {
      return;
    }

    if (!isset($this->rendered_fields)) {
      $this->rendered_fields = array();
      $this->view->row_index = 0;
      $keys = array_keys($this->view->field);
      foreach ($result as $count => $row) {
        $this->view->row_index = $count;
        foreach ($keys as $id) {
          $this->rendered_fields[$id] = $this->view->field[$id]->theme($row);
        }

        $this->row_tokens = $this->view->field[$id]->get_render_tokens(array());
      }
      unset($this->view->row_index);
    }

    return $this->rendered_fields;
  }


 function validate() {
    $errors = parent::validate();

    if ($this->uses_row_plugin()) {
      $plugin = $this->display->handler->get_plugin('row');
      if (empty($plugin)) {
        $errors[] = t('Style @style requires a row style but the row plugin is invalid.', array('@style' => $this->definition['title']));
      }
      else {
        $result = $plugin->validate();
        if (!empty($result) && is_array($result)) {
          $errors = array_merge($errors, $result);
        }
      }
    }
    return $errors;
  }

  function query() {
    parent::query();
    if (isset($this->row_plugin)) {
      $this->row_plugin->query();
    }
  }


  /**
   * Render the display in this style.
   */
  function render() {

    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    $fields = $this->render_fields($this->view->result);
    
    // Render each group separately and concatenate.  Plugins may override this
    // method if they wish some other way of handling grouping.
    $output = '';
	$flag=1;
    foreach ($sets as $title => $records) {
      if ($this->uses_row_plugin()) {
        $rows = array();
        foreach ($records as $row_index => $row) {
          $this->view->row_index = $row_index;

          $node_row = $row->_field_data['nid']['entity']; // isolate the node data in an easy to call variable

          $sss_views_data = array(); // an array to hold sss_views data to be pass to be pass through the row plugin to the row template

            // loop through the Views fields
          foreach($fields AS $field_key => $field_value ) {

              // if the field isset AND the field has a value set
              // this check is necessary, since not all nodes will have every B-AWS path set for every field
              // it also helps avoids undefined variable warnings
            if (  strpos($field_key , 'sss'))
            if ( isset( $node_row->{$field_key}) && isset( $node_row->{$field_key}['und'][0]['value']) && strpos($field_key , 'sss')) {
              $sss_views_data[] = sss_soap_get_trend(
                $node_row->field_sss_server['und'][0]['value'],
                $node_row->field_sss_login_name['und'][0]['value'],
                $node_row->field_sss_login_password['und'][0]['value'],
                $node_row->{$field_key}['und'][0]['value'],
                $this->options['interval'],
                $this->options['max_records'],
                $this->options['end_date_range']
              );
			  $flag=0;
            } // end of if, the field contains a B-AWS path value
          
          } // end of foreach, field

            // pass to the row plugin, the isolated row's node data, the selected field values, and the rendered sss_views data
            // store in a $rows variable, consisting of the themed rows, which will later be passed to the Views style template
			$render=array();
			$render['row']= $row->_field_data['nid']['entity'];
			$render['field']= $fields;
			$render['sss_views_data']=  $sss_views_data;
			
          $rows[$row_index] = $this->row_plugin->render($render);

        }
      }
      else {
        $rows = $records;
      }

      $output .= theme($this->theme_functions(), array(
        'view' => $this->view,
        'options' => $this->options,
        'rows' => $rows,
        'title' => $title)
      );

    } // end of foreach
		  if($flag){
			drupal_set_message("Please add a soap path expression field ");
			}
    unset($this->view->row_index);
    return $output;

  }

}


/**
 * Display the view as an HTML list element
 */
function template_preprocess_sss_views_view_style_trend(&$vars) {
/**
  $handler  = $vars['view']->style_plugin;

  $vars['units'] = $handler->options['units'];
  $vars['interval'] = $handler->options['interval'];
  $vars['timestamp_adjustment'] = $handler->options['timestamp_adjustment'];
  $vars['max_records'] = $handler->options['max_records'];
  $vars['end_date_range'] = $handler->options['end_date_range'];
  template_preprocess_views_view_unformatted($vars);
 
 */
}
